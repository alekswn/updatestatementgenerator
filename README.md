# Pipeline status: ![status image](https://gitlab.com/alekswn/updatestatementgenerator/badges/master/pipeline.svg)

# How to run the script:
Run unit-test: `python -m unittest`

Using CLI driver: `update_statement_generator.py [-h] -i ORIGINAL_DOCUMENT [-u MUTATIONS]`

`ORIGINAL_DOCUMENT` is the filename of an original document in JSON format.

`MUTATIONS` is an optional argument, a filename of mutations. By default, the list of mutations is read from STDIN. The
the script accepts multiline JSON and ignores comments (any non-valid JSON lines).


# General approach:
The general idea is to convert JSON to an internal tree-like representation for the original document and mutation list.
A node for document tree is (`class DocArray`) represents a list of documents. The root document is represented as a list with a single entry.
Lists are converted to hashmaps indexed by `_id` fields.
Mutations are represented as object tree. Each kind of mutation has its own class.
`$add` and `$remove` mutations can be leaf nodes only.
`$update` mutation represent non-leafs nodes and the root node.
Non-leaf nodes do not produce any output if there were no fields updated.
We iterate over both trees using recursion.
The mutation tree acts as a filter to select nodes from the document tree.
The tree traversal procedure produces a merged dictionary with mutation statements.

# Computational complexity:
We do the following operations in a sequence:

1. Parse original document JSON ( `~O(N)` where `N` is the original document JSON size)
2. Converting Python's JSON representation to custom tree-like index ( `~O(N)`)
3. Parsing mutation JSON ( `~O(M)` where `M` is the mutation size )
4. Converting parsed JSON to tree-like custom structure ( `~O(M)`)
5. Traversing mutation and document tree ( `~O(M)` )

Therefore overall computational complexity is `~O(N+M)`.
Assuming N is much greater than M the performance bottlenecks are gonna be in original document parsing and indexing.


# TODO List:

- Sanitize input. Currently, an incorrect mutation can break logic.
- Lazy indexing of the original document. If the original document was large mutations would touch just a small part of the document. However, we index the whole document currently. It's likely to be a performance bottleneck.
