#!/usr/bin/env python

from setuptools import setup

setup(name='Update Statement Generator',
      version='0.1',
      py_modules=['update_statement_generator'],
      url='https://gitlab.com/alekswn/updatestatementgenerator',
      author='Alexey Novikov',
      author_email='spam@novikov.io',
      )
