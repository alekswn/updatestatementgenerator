#!/usr/bin/env python

import argparse
import json
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('generate_update_statement')

RESERVED_KEYS = set(['_id', '_delete'])


class DocumentException(Exception):
    pass


# A tree node which holds list of documents
# Each node can resolve statement update paths for fields in documents stored in the list
class DocArray:

    # Constructor expects an array of dicts in general case.
    # `path` argument is update statement path of a parent node.
    # Root node can be passed as plain dictionary with `path=None`.
    def __init__(self, root, path=None):
        self._path = path
        self._children = {}

        wrapped_root = root if type(root) == list else [root]
        for i in range(len(wrapped_root)):
            node = wrapped_root[i]
            if type(node) == dict and '_id' in node:
                kv = {k: DocArray(v, self._build_path(path, i, k))
                      for k, v in node.items() if self.isdocarray(v)}
                self._children[node['_id']] = (i, kv)

    @staticmethod
    def _build_path(prefix, index, field):
        if prefix is None:
            return field or ''
        return '.'.join(filter(None, [prefix, '{}'.format(index), field]))

    def _get_index(self, arr_id):
        if arr_id is None:
            return 0
        index, _ = self._children[arr_id]
        return index

    # Returns a nested subarray
    def get_subarray(self, arr_id, field_name):
        if arr_id is None:
            _, kv = list(self._children.values())[0]
        else:
            _, kv = self._children[arr_id]
        return kv[field_name]

    # Returns it's own path for update statement
    def get_path(self):
        return self._path if self._path else ''

    # Returns update statement path for a document stored in the array
    def resolve_arr_id_path(self, arr_id):
        return self._build_path(self._path, self._get_index(arr_id), None)

    # Returns update statement path of a field of document stored in array
    def resolve_field_path(self, arr_id, field_name):
        return self._build_path(self._path, self._get_index(arr_id), field_name)

    # Checks if an object can be represented as DocArray
    # This is the most primitive implementation.
    @classmethod
    def isdocarray(cls, json_node):
        return type(json_node) == list


class MutationException(Exception):
    pass


# A node in mutation tree to represent `$add` mutation.
# Responsible for producing correct update statement for the mutation.
# Can be a leaf node of a mutation tree.
class MutationAdd:
    def __init__(self, mutation):
        self._arr = mutation

    def __call__(self, docarray):
        kv = {docarray.get_path(): [self._arr]}
        return {'$add': kv}


# A node in mutation tree to represent `$remove` mutation.
# Responsible for producing correct update statement for the mutation.
# Can be a leaf node of a mutation tree.
class MutationRemove:
    def __init__(self, mutation):
        self._id = mutation['_id']

    def __call__(self, docarray):
        kv = {docarray.resolve_arr_id_path(self._id): True}
        return {'$remove': kv}


# A node in mutation tree to represent `$update` mutation.
# Responsible for producing correct update statement for the mutation and merging
# statements produced by it's children.
# Can be a any node of a mutation tree. Root node is represented by this class.
class MutationUpdate:
    def __init__(self, mutation):
        self._id = None
        self._kv = {}
        self._children = {}

        for k, v in mutation.items():
            if k == '_id':
                self._id = v
            else:
                if DocArray.isdocarray(v):
                    self._children[k] = [_make_mutation(i) for i in v]
                else:
                    self._kv[k] = v

    def __call__(self, docarray):
        kv = {docarray.resolve_field_path(
            self._id, k): v for k, v in self._kv.items()}
        statements = {'$update': kv} if kv else {}
        for k, v in self._children.items():
            subdocarr = docarray.get_subarray(self._id, k)
            for i in v:
                statements = _merge_statements(statements, i(subdocarr))
        return statements


def _merge_statements(s1, s2):
    ret = {}
    for k in s1.keys() & s2.keys():
        ret[k] = s1[k] | s2[k]
    for k in s1.keys() - s2.keys():
        ret[k] = s1[k]
    for k in s2.keys() - s1.keys():
        ret[k] = s2[k]
    return ret


# A factory method to create a node of mutation tree
def _make_mutation(obj):
    if '_id' in obj:
        if '_delete' in obj and obj['_delete']:
            return MutationRemove(obj)
        return MutationUpdate(obj)
    return MutationAdd(obj)


# The method to implement
def generate_update_statement(original_document_json, mutation_json):
    mutation = MutationUpdate(json.loads(mutation_json))
    docarray = DocArray(json.loads(original_document_json))
    update_statement = mutation(docarray)
    return json.dumps(update_statement)


# CLI driver
def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--original-document",

                        help="A JSON file with original document.",
                        required=True,
                        type=argparse.FileType('r'))
    parser.add_argument("-u", "--mutations",
                        help="A file with mutations. Multiline JSON accepted. Lines which can not be parsed as JSON (i.e. comments) are skipped.",
                        default="stdin",
                        type=argparse.FileType('r'))

    args = parser.parse_args()

    original_document = args.original_document.read()

    for line in args.mutations:
        try:
            json.loads(line)
        except json.JSONDecodeError:
            logger.debug(
                "JSON Exception. Skipping line on mutation list: " + line)
        statement = generate_update_statement(original_document, line)
        print(statement)


if __name__ == "__main__":
    main()
