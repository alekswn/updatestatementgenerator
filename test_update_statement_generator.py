import unittest
import json

from update_statement_generator import generate_update_statement, DocArray, MutationUpdate


class TestMutationUpdate(unittest.TestCase):

    def test_ok(self):
        self.assertTrue('True')

    def test_update_root(self):
        mutation = MutationUpdate({'foo': 'bar'})
        document = DocArray({'_id': 42})
        expected = {'$update': {'foo': 'bar'}}
        actual = mutation(document)
        self.assertEqual(actual, expected)

    def test_multiupdate_root(self):
        mutation = MutationUpdate({'val1': 'val1', 'val2': 'val2'})
        document = DocArray({'_id': 42})
        expected = {'$update': {'val1': 'val1', 'val2': 'val2'}}
        actual = mutation(document)
        self.assertEqual(actual, expected)

    def test_multilevel_update(self):
        mutation = MutationUpdate(
            {'val0': 'val0', 'level1': [{'_id': 1, 'val1': 'val1', 'level2': [{'_id': 2, 'val2': 'val2'}]}]})
        document = DocArray(
            {'_id': 0, 'level1': [{'_id': 1, 'level2': [{'_id': 2}]}]})
        expected = {'$update': {'val0': 'val0',
                                'level1.0.val1': 'val1', 'level1.0.level2.0.val2': 'val2'}}
        actual = mutation(document)
        self.assertEqual(actual, expected)


class TestMutationRemove(unittest.TestCase):

    def test_ok(self):
        self.assertTrue('True')

    def test_remove_simple(self):
        mutation = MutationUpdate({'level1': [{'_id': 1, '_delete': True}]})
        document = DocArray(
            {'_id': 0, 'level1': [{'_id': 1, 'level2': [{'_id': 2}]}]})
        expected = {'$remove': {'level1.0': True}}
        actual = mutation(document)
        self.assertEqual(actual, expected)

    def test_remove_nested(self):
        mutation = MutationUpdate(
            {'level1': [{'_id': 1, 'level2': [{'_id': 2, '_delete': True}]}]})
        document = DocArray(
            {'_id': 0, 'level1': [{'_id': 1, 'level2': [{'_id': 2}]}]})
        expected = {'$remove': {'level1.0.level2.0': True}}
        actual = mutation(document)
        self.assertEqual(actual, expected)

    def test_remove_multilevel(self):
        mutation = MutationUpdate(
            {'level1': [{'_id': 1, 'level2': [{'_id': 2, '_delete': True}]}, {'_id': 11, '_delete': True}]})
        document = DocArray(
            {'_id': 0, 'level1': [{'_id': 1, 'level2': [{'_id': 2}]}, {'_id': 11}]})
        expected = {'$remove': {'level1.0.level2.0': True, 'level1.1': True}}
        actual = mutation(document)
        self.assertEqual(actual, expected)


class TestMutationAdd(unittest.TestCase):

    def test_ok(self):
        self.assertTrue('True')

    def test_add_simple(self):
        mutation = MutationUpdate({'level1': [{'val1': 'val1'}]})
        document = DocArray(
            {'_id': 0, 'level1': [{'_id': 1, 'level2': [{'_id': 2}]}]})
        expected = {'$add': {'level1': [{'val1': 'val1'}]}}
        actual = mutation(document)
        self.assertEqual(actual, expected)

    def test_add_nested(self):
        mutation = MutationUpdate(
            {'level1': [{'_id': 1, 'level2': [{'val2': 'val2'}]}]})
        document = DocArray(
            {'_id': 0, 'level1': [{'_id': 1, 'level2': [{'_id': 2}]}]})
        expected = {'$add': {'level1.0.level2': [{'val2': 'val2'}]}}
        actual = mutation(document)
        self.assertEqual(actual, expected)

    def test_add_multilevel(self):
        mutation = MutationUpdate(
            {'level1': [{'_id': 1, 'level2': [{'val2': 'val2'}]}, {'val1': 'val1'}]})
        document = DocArray(
            {'_id': 0, 'level1': [{'_id': 1, 'level2': [{'_id': 2}]}, {'_id': 11}]})
        expected = {'$add': {'level1.0.level2': [
            {'val2': 'val2'}], 'level1': [{'val1': 'val1'}]}}
        actual = mutation(document)
        self.assertEqual(actual, expected)


class TestMutationMixed(unittest.TestCase):
    def test_add_update_remove_ok(self):
        mutation = MutationUpdate({'posts': [
            {"_id": 2, "value": "too"},
            {"value": "four"},
            {"_id": 4, "_delete": True}
        ]})
        document = DocArray({'_id': 42, 'foo': 'zoo', 'posts': [
                                                      {"_id": 2, "value": "one"},
                                                      {"value": "four"},
                                                      {"_id": 4, "value": "three"}
        ]})
        expected = {
            "$update": {"posts.0.value": "too"},
            "$add": {"posts": [{"value": "four"}]},
            "$remove": {"posts.2": True}
        }
        actual = mutation(document)
        self.assertEqual(actual, expected)

    def test_add_update_remove_multilevel(self):
        mutation = MutationUpdate({'posts': [
            {"_id": 2, "value": "too"},
            {"_id": 3, "mentions": [{"value": "four"}]},
            {"_id": 4, "_delete": True}
        ]})
        document = DocArray({'_id': 42, 'foo': 'zoo', 'posts': [
                                                      {"_id": 2, "value": "one"},
                                                      {"_id": 3, "mentions": [
                                                          {"_id": 12, "value": "zero"}]},
                                                      {"_id": 4, "value": "three"}
        ]})
        expected = {
            "$update": {"posts.0.value": "too"},
            "$add": {"posts.1.mentions": [{"value": "four"}]},
            "$remove": {"posts.2": True}
        }
        actual = mutation(document)
        self.assertEqual(actual, expected)


class TestDocArray(unittest.TestCase):

    def test_ok(self):
        self.assertTrue('True')

    def test_isdocarray(self):
        self.assertTrue(DocArray.isdocarray([]))
        self.assertFalse(DocArray.isdocarray({}))
        self.assertFalse(DocArray.isdocarray('foo'))
        self.assertFalse(DocArray.isdocarray(42))
        self.assertFalse(DocArray.isdocarray(True))

    def test_subarray_path(self):
        docarray = DocArray(
            {'_id': 0, 'level1': [{'_id': 1, 'level2': [{'_id': 2}]}, {'_id': 11}]})
        self.assertEqual(docarray.get_path(), '')
        self.assertEqual(docarray.get_subarray(
            None, 'level1').get_path(), 'level1')
        self.assertEqual(docarray.get_subarray(
            0, 'level1').get_path(), 'level1')
        self.assertEqual(docarray.get_subarray(None, 'level1').get_subarray(
            1, 'level2').get_path(), 'level1.0.level2')

    def test_resolve_arr_id_path(self):
        docarray = DocArray(
            [{'_id': 1, 'level2': [{'_id': 2}]}, {'_id': 11}], 'level1')
        self.assertEqual(docarray.resolve_arr_id_path(1), 'level1.0')

    def test_resolve_field_path(self):
        docarray = DocArray(
            [{'_id': 1, 'level2': [{'_id': 2}]}, {'_id': 11}], 'level1')
        self.assertEqual(docarray.resolve_field_path(1, 'foo'), 'level1.0.foo')


class TestAcceptance(unittest.TestCase):

    def assertJsonEqual(self, actual, expected):
        self.assertEqual(json.loads(actual), json.loads(expected))

    def setUp(self):
        with open('test_data/sample_input.json') as f:
            self.original_document_json = f.read()

    def test_ok(self):
        self.assertTrue('True')

    # //INPUT: Update value field of post with _id of 2
    # //OUTPUT: Update value field of post at index 0
    def test_update_simple(self):
        mutation_json = '{ "posts": [{"_id": 2, "value": "two"}] }'
        expected_json = '{ "$update": {"posts.0.value": "two"} }'
        actual_json = generate_update_statement(
            self.original_document_json, mutation_json)
        self.assertJsonEqual(actual_json, expected_json)

    # //INPUT: Update text field in mention with _id of 5, for post with _id of 3
    # //OUTPUT: Update text field in mention at index 1, for post at index 0
    def test_update_nested(self):
        mutation_json = '{ "posts": [{"_id": 3, "mentions": [ {"_id": 5, "text": "pear"}]}] }'
        expected_json = '{ "$update": {"posts.1.mentions.0.text": "pear"}}'
        actual_json = generate_update_statement(
            self.original_document_json, mutation_json)
        self.assertJsonEqual(actual_json, expected_json)

    # //INPUT: Add post; notice that there is no _id because the post doesn't exist yet
    # //OUTPUT: Add post
    def test_post_simple(self):
        mutation_json = '{"posts": [{"value": "four"}] }'
        expected_json = '{"$add": {"posts": [{"value": "four"}] }}'
        actual_json = generate_update_statement(
            self.original_document_json, mutation_json)
        self.assertJsonEqual(actual_json, expected_json)

    # //INPUT: Add mention to post with _id of 3
    # //OUTPUT: Add mention for post at index 2
    def test_post_nested(self):
        mutation_json = '{"posts": [{"_id": 3, "mentions": [{"text": "banana"}]}]}'
        expected_json = '{"$add": {"posts.1.mentions": [{"text": "banana"}]}}'
        actual_json = generate_update_statement(
            self.original_document_json, mutation_json)
        self.assertJsonEqual(actual_json, expected_json)

    # //INPUT: Remove post with _id of 2
    # //OUTPUT: Remove post at index 0
    def test_remove_simple(self):
        mutation_json = '{ "posts": [{"_id": 2, "_delete": true}] }'
        expected_json = '{ "$remove" : {"posts.0" : true} }'
        actual_json = generate_update_statement(
            self.original_document_json, mutation_json)
        self.assertJsonEqual(actual_json, expected_json)

    # //INPUT: Remove mention with _id of 6, for post with _id of 3
    # //OUTPUT: Remove mention at index 1, for post at index 1
    def test_remove_nested(self):
        mutation_json = '{ "posts": [{"_id": 3, "mentions": [{"_id": 6, "_delete": true}]}]}'
        expected_json = '{ "$remove" : {"posts.1.mentions.1": true}}'
        actual_json = generate_update_statement(
            self.original_document_json, mutation_json)
        self.assertJsonEqual(actual_json, expected_json)

    def test_update_add_remove_in_single_statement(self):
        mutation_json = '{"posts": [{"_id": 2, "value": "too"}, {"value": "four"}, {"_id": 4, "_delete": true}]}'
        expected_json = '{"$update": {"posts.0.value": "too"},"$add": {"posts": [{"value": "four"}] }, "$remove" : {"posts.2" : true}}'
        actual_json = generate_update_statement(
            self.original_document_json, mutation_json)
        self.assertJsonEqual(actual_json, expected_json)


if __name__ == '__main__':
    unittest.main()
